const fruitsData = require("./3-arrays-vitamins.cjs")

const availableFruits = fruitsData.filter((fruit)=>{
    if(fruit.available === true){
        return fruit
    }
})

console.log(availableFruits)